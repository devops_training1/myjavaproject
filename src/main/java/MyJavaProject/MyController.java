package MyJavaProject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
public final class MyController {

    @GetMapping
    public final String bonjour() throws UnknownHostException {
        return "Bonjour!" + InetAddress.getLocalHost().getHostAddress();
    }
}